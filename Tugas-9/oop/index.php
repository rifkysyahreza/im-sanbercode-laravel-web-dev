<?php
// include 'animal.php';
include 'ape.php';


$sheep = new Animal("shaun");


echo "Name: $sheep->name <br>"; // "shaun"
echo "Legs: $sheep->legs <br>"; // 4
echo "Cold Blooded: $sheep->cold_blooded <br>"; // "no"
echo "<br>";


$kodok = new Frog("buduk");

echo "Name: $kodok->name <br>";
echo "Legs: $kodok->legs <br>";
echo "Cold Blooded: $kodok->cold_blooded <br>";
echo "Jump: ";$kodok->jump(); // "hop hop"
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");

echo "Name: $sungokong->name <br>";
echo "Legs: $sungokong->legs <br>";
echo "Cold Blooded: $sungokong->cold_blooded <br>";
echo "Yell: "; $sungokong->yell(); // "Auooo"
?>